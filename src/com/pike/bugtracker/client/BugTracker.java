package com.pike.bugtracker.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.RootLayoutPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class BugTracker implements EntryPoint {

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		MainView view = new MainView();
		RootLayoutPanel.get().add(view);
		Command cmd = new UpdateTableCommand(view);
		cmd.execute();
	}
}
