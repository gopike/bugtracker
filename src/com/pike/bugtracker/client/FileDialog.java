package com.pike.bugtracker.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteHandler;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.VerticalPanel;

public class FileDialog extends DialogBox {
	public FileDialog(final MainView view) {
		setHTML("Import File");

		final FormPanel formPanel = new FormPanel();
		formPanel.addSubmitCompleteHandler(new SubmitCompleteHandler() {
			public void onSubmitComplete(SubmitCompleteEvent event) {
				Command cmd = new UpdateTableCommand(view);
				hide();
				cmd.execute();
			}
		});
		formPanel.setMethod(FormPanel.METHOD_POST);
		formPanel.setAction("/bugtracker/upload");
		formPanel.setEncoding(FormPanel.ENCODING_MULTIPART);
		setWidget(formPanel);
		formPanel.setSize("100%", "100%");

		VerticalPanel verticalPanel = new VerticalPanel();
		formPanel.setWidget(verticalPanel);
		verticalPanel.setSize("384px", "163px");

		FileUpload fileUpload = new FileUpload();
		fileUpload.setName("uploadFormElement");
		verticalPanel.add(fileUpload);

		Button btnNewButton = new Button("New button");
		btnNewButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				formPanel.submit();
			}
		});
		btnNewButton.setText("Import");
		verticalPanel.add(btnNewButton);
		verticalPanel.setCellVerticalAlignment(btnNewButton,
				HasVerticalAlignment.ALIGN_MIDDLE);
		verticalPanel.setCellHorizontalAlignment(btnNewButton,
				HasHorizontalAlignment.ALIGN_CENTER);
	}

}
