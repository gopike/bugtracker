package com.pike.bugtracker.client;

import java.util.Date;

import com.google.gwt.cell.client.DateCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.pike.bugtracker.shared.DailyStat;

public class MainView extends Composite {

	private static MainViewUiBinder uiBinder = GWT
			.create(MainViewUiBinder.class);

	interface MainViewUiBinder extends UiBinder<Widget, MainView> {
	}

	// Add a date column to show the date.
	DateCell dateCell = new DateCell();

	Column<DailyStat, Date> dateColumn = new Column<DailyStat, Date>(dateCell) {
		@Override
		public Date getValue(DailyStat stat) {
			return new Date(stat.getDate().getTime());
		}
	};

	// Add a text column to show sev1.
	TextColumn<DailyStat> sev1Column = new TextColumn<DailyStat>() {
		@Override
		public String getValue(DailyStat stat) {
			return String.valueOf(stat.getSev1());
		}
	};

	// Add a text column to show sev2.
	TextColumn<DailyStat> sev2Column = new TextColumn<DailyStat>() {
		@Override
		public String getValue(DailyStat stat) {
			return String.valueOf(stat.getSev2());
		}
	};

	// Add a text column to show sev3.
	TextColumn<DailyStat> sev3Column = new TextColumn<DailyStat>() {
		@Override
		public String getValue(DailyStat stat) {
			return String.valueOf(stat.getSev3());
		}
	};

	// Add a text column to show total.
	TextColumn<DailyStat> totalColumn = new TextColumn<DailyStat>() {
		@Override
		public String getValue(DailyStat stat) {
			return String.valueOf(stat.getTotal());
		}
	};

	// Add a text column to show verify.
	TextColumn<DailyStat> verifyColumn = new TextColumn<DailyStat>() {
		@Override
		public String getValue(DailyStat stat) {
			return String.valueOf(stat.getVerify());
		}
	};

	// Add a text column to show incoming.
	TextColumn<DailyStat> incomingColumn = new TextColumn<DailyStat>() {
		@Override
		public String getValue(DailyStat stat) {
			return String.valueOf(stat.getIncoming());
		}
	};

	// Add a text column to show untargeted.
	TextColumn<DailyStat> untargetedColumn = new TextColumn<DailyStat>() {
		@Override
		public String getValue(DailyStat stat) {
			return String.valueOf(stat.getUntargeted());
		}
	};

	// Add a text column to show Deferred.
	TextColumn<DailyStat> deferredColumn = new TextColumn<DailyStat>() {
		@Override
		public String getValue(DailyStat stat) {
			return String.valueOf(stat.getDeferred());
		}
	};

	// Add a selection model to handle user selection.
	final SingleSelectionModel<DailyStat> selectionModel = new SingleSelectionModel<DailyStat>();

	ListDataProvider<DailyStat> dataProvider = new ListDataProvider<DailyStat>();

	@UiField(provided = true)
	DataGrid<DailyStat> dataGrid = new DataGrid<DailyStat>();
	@UiField MenuItem importMenuItem;
	@UiField MenuItem average;

	public MainView() {
		initWidget(uiBinder.createAndBindUi(this));
		dataGrid.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		dataGrid.addColumn(dateColumn, "Date");
		dataGrid.addColumn(sev1Column, "Sev 1");
		dataGrid.addColumn(sev2Column, "Sev 2");
		dataGrid.addColumn(sev3Column, "Sev 3");
		dataGrid.addColumn(totalColumn, "Total Open Bugs");
		dataGrid.addColumn(verifyColumn, "To Verify");
		dataGrid.addColumn(incomingColumn, "Incoming");
		dataGrid.addColumn(untargetedColumn, "Untargeted");
		dataGrid.addColumn(deferredColumn, "Deferred");
		dataGrid.setSelectionModel(selectionModel);

		selectionModel
				.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
					public void onSelectionChange(SelectionChangeEvent event) {
						DailyStat selected = selectionModel.getSelectedObject();
						if (selected != null) {
							Window.alert("You selected: "
									+ selected.getDate().toString());
						}
					}
				});

	    // Connect the table to the data provider.
	    dataProvider.addDataDisplay(dataGrid);
	    
	    importMenuItem.setCommand(new Command() {
			
			@Override
			public void execute() {
				FileDialog dialog = new FileDialog(MainView.this);
				dialog.center();
			}
		});
	}

	public DataGrid<DailyStat> getDataGrid() {
		return dataGrid;
	}

	public ListDataProvider<DailyStat> getDataProvider() {
		return dataProvider;
	}
	
	public void setAverage(double avg) {
		average.setText("Average: " + String.valueOf(avg));
	}
}
