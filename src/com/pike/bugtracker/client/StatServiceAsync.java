package com.pike.bugtracker.client;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.pike.bugtracker.shared.DailyStat;

public interface StatServiceAsync {

	public void getStats(AsyncCallback<List<DailyStat>> callback);
}
