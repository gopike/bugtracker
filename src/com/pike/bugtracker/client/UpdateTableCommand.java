package com.pike.bugtracker.client;

import java.util.List;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.pike.bugtracker.shared.DailyStat;

public class UpdateTableCommand implements Command {
	MainView view;

	public UpdateTableCommand(MainView view) {
		super();
		this.view = view;
	}

	@Override
	public void execute() {
		AsyncCallback<List<DailyStat>> callback = new AsyncCallback<List<DailyStat>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO add error handling

			}

			@Override
			public void onSuccess(List<DailyStat> result) {
				List<DailyStat> list = view.getDataProvider().getList();
				list.clear();
				int total = 0;
				int count = 0;
				for (DailyStat stat : result) {
					list.add(stat);
					total += stat.getTotal();
					count ++;
				}
				view.setAverage(total/count);

			}
		};

		StatService.Util.getInstance().getStats(callback);
	}

}
