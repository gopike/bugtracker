/*
 * Copyright 2011 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pike.bugtracker.server;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.pike.bugtracker.client.StatService;
import com.pike.bugtracker.server.dao.StatDao;
import com.pike.bugtracker.shared.DailyStat;

public class StatServiceImpl extends RemoteServiceServlet implements StatService {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4523553538049648241L;
	
	StatDao dao;
	
	@Override
	public List<DailyStat> getStats() {
		if(dao == null) {
			ApplicationContext beanFactory =
				    WebApplicationContextUtils
				        .getRequiredWebApplicationContext(getServletContext());
			dao= beanFactory.getBean(StatDao.class);
		}
		return dao.getAll();
	}
}
