package com.pike.bugtracker.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.text.ParseException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.pike.bugtracker.server.dao.StatDao;

/**
 * Upload Servlet - This calss is responsible for recieving an uploaded csv file
 * and parsing the bug tracking records
 * 
 * @author Gordon
 * 
 */
public class UploadServlet extends HttpServlet {
	StatDao dao;

	/**
	 * 
	 */
	private static final long serialVersionUID = -5826507230846641354L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if(dao == null) {
			ApplicationContext beanFactory =
				    WebApplicationContextUtils
				        .getRequiredWebApplicationContext(getServletContext());
			dao= beanFactory.getBean(StatDao.class);
		}

		try {
			List<FileItem> items = new ServletFileUpload(
					new DiskFileItemFactory()).parseRequest(request);
			for (FileItem item : items) {
				if (item.isFormField()) {
					String fieldname = item.getFieldName();
					String fieldvalue = item.getString();
				} else {
					// Process form file field (input type="file").
					String fieldname = item.getFieldName();
					String filename = FilenameUtils.getName(item.getName());
					InputStream filecontent = item.getInputStream();
					parseFile(filecontent);
				}
			}
		} catch (FileUploadException e) {
			throw new ServletException("Cannot parse multipart request.", e);
		}
	}

	private void parseFile(InputStream fileContent) {
		LineNumberReader reader = new LineNumberReader(new InputStreamReader(
				fileContent));

		try {
			boolean firstLine = true;
			String line;
			do {
				line = reader.readLine();
				if (firstLine) {
					firstLine = false;
					continue;
				}
				if((line != null) && (line.trim().length() > 0))
					dao.parseAndSave(line);
			} while (line != null);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Date,Sev1,Sev2,Sev3,Total Open Bugs,To
	// Verify,Incoming,Untargeted,Deferred
	// 12/9/2011,5,220,44,269,182,5,15,61
	// 12/12/2011,5,209,44,258,185,2,0,61
	// 12/13/2011,5,249,61,315,119,18,45,63

}
