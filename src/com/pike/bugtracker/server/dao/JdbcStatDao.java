package com.pike.bugtracker.server.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.pike.bugtracker.shared.DailyStat;

/**
 * Implementation of StatsDao that uses JdbcTemplate to carry out requests.
 * 
 * @author Gordon
 * 
 */
@Component
public class JdbcStatDao implements StatDao {
	String dropSql = "drop table if exists stats";
	String createSql = "create table stats (id integer, date date, sev1 integer, sev2 integer, sev3 integer, total integer, verify integer, incoming integer, untargeted integer, deferred integer)";
	String getAllSql = "select * from stats";
	String getByDateSql = "select * from stats where date=?";
	String saveSql = "INSERT INTO stats (date,sev1,sev2,sev3,total,verify,incoming,untargeted,deferred) VALUES (?,?,?,?,?,?,?,?,?)";

	@Autowired
	DataSource dataSource;

	JdbcTemplate template;

	private static int[] insertTypes = new int[] { java.sql.Types.DATE,
			java.sql.Types.INTEGER, java.sql.Types.INTEGER,
			java.sql.Types.INTEGER, java.sql.Types.INTEGER,
			java.sql.Types.INTEGER, java.sql.Types.INTEGER,
			java.sql.Types.INTEGER, java.sql.Types.INTEGER };

	/**
	 * Row Mapper to create a stat given the resultset of each row found.
	 */
	private RowMapper<DailyStat> mapper = new RowMapper<DailyStat>() {

		@Override
		public DailyStat mapRow(ResultSet rs, int row) throws SQLException {
			DailyStat stat = new DailyStat();
			stat.setDate(rs.getDate("date"));
			stat.setSev1(rs.getInt("sev1"));
			stat.setSev2(rs.getInt("sev2"));
			stat.setSev3(rs.getInt("sev3"));
			stat.setTotal(rs.getInt("total"));
			stat.setVerify(rs.getInt("verify"));
			stat.setIncoming(rs.getInt("incoming"));
			stat.setUntargeted(rs.getInt("untargeted"));
			stat.setDeferred(rs.getInt("deferred"));
			return stat;
		}
	};

	@PostConstruct
	public void init() {
		template = new JdbcTemplate(dataSource);
		template.execute(dropSql);
		template.execute(createSql);

	}

	@Override
	public List<DailyStat> getAll() {
		return template.query(getAllSql, mapper);
	}

	@Override
	public void put(DailyStat stat) {
		Object[] params = objectList(stat);
		template.update(saveSql, params, insertTypes);
	}

	private Object[] objectList(DailyStat stat) {
		return new Object[] { stat.getDate(), stat.getSev1(), stat.getSev2(),
				stat.getSev3(), stat.getTotal(), stat.getVerify(),
				stat.getIncoming(), stat.getUntargeted(), stat.getDeferred() };
	}

	@Override
	public void parseAndSave(String row) throws ParseException {
		DailyStat stat = parse(row);
		if(!isAlreadyImported(stat))
			put(stat);
	}

	public boolean isAlreadyImported(DailyStat stat) {
		List<DailyStat> results = template.query(getByDateSql,
				new Object[] { stat.getDate() }, mapper);
		if (results.size() > 0)
			return true;
		else
			return false;
	}

	// Date,Sev1,Sev2,Sev3,Total Open Bugs,To
	// Verify,Incoming,Untargeted,Deferred
	// 12/9/2011,5,220,44,269,182,5,15,61
	// 12/12/2011,5,209,44,258,185,2,0,61
	// 12/13/2011,5,249,61,315,119,18,45,63

	public DailyStat parse(String row) throws ParseException {
		String[] cols = row.split(",");
		DailyStat stat = new DailyStat();
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		Date date = (Date) formatter.parse(cols[0]);

		stat.setDate(new java.sql.Date(date.getTime()));
		stat.setSev1(Integer.parseInt(cols[1]));
		stat.setSev2(Integer.parseInt(cols[2]));
		stat.setSev3(Integer.parseInt(cols[3]));
		stat.setTotal(Integer.parseInt(cols[4]));
		stat.setVerify(Integer.parseInt(cols[5]));
		stat.setIncoming(Integer.parseInt(cols[6]));
		stat.setUntargeted(Integer.parseInt(cols[7]));
		stat.setDeferred(Integer.parseInt(cols[8]));

		return stat;
	}

}
