package com.pike.bugtracker.server.dao;

import java.text.ParseException;
import java.util.List;

import com.pike.bugtracker.shared.DailyStat;

public interface StatDao {

	public List<DailyStat> getAll();
	
	public void put(DailyStat stat);
	
	public void parseAndSave(String row) throws ParseException;
}
