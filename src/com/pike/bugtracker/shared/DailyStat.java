package com.pike.bugtracker.shared;

import java.io.Serializable;
import java.sql.Date;



/**
 * Class that hold s the daily bug tracking stats
 * @author Gordon
 *
 */
public class DailyStat implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 569298994967525622L;
	
	private Date date;
	private int sev1;
	private int sev2;
	private int sev3;
	private int total;
	private int verify;
	private int incoming;
	private int untargeted;
	private int deferred;
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getSev1() {
		return sev1;
	}
	public void setSev1(int sev1) {
		this.sev1 = sev1;
	}
	public int getSev2() {
		return sev2;
	}
	public void setSev2(int sev2) {
		this.sev2 = sev2;
	}
	public int getSev3() {
		return sev3;
	}
	public void setSev3(int sev3) {
		this.sev3 = sev3;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getVerify() {
		return verify;
	}
	public void setVerify(int verify) {
		this.verify = verify;
	}
	public int getIncoming() {
		return incoming;
	}
	public void setIncoming(int incoming) {
		this.incoming = incoming;
	}
	public int getUntargeted() {
		return untargeted;
	}
	public void setUntargeted(int untargeted) {
		this.untargeted = untargeted;
	}
	public int getDeferred() {
		return deferred;
	}
	public void setDeferred(int deferred) {
		this.deferred = deferred;
	}
	
}
