package com.pike.bugtracker.server.dao;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.pike.bugtracker.shared.DailyStat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:**/context.xml"})
public class JdbcStatDaoTest {
	@Autowired StatDao dao;
	
	@Autowired DataSource source;
	
	@Test
	public void test() {
		
		DailyStat stat = new DailyStat();
		stat.setDate(new java.sql.Date(new Date().getTime()));
		stat.setSev1(1);
		stat.setSev2(1);
		stat.setSev3(1);
		stat.setTotal(1);
		stat.setUntargeted(1);
		stat.setVerify(1);
		stat.setDeferred(1);
		stat.setIncoming(1);
		
		dao.put(stat);
		
		List<DailyStat> stats = dao.getAll();
		DailyStat result = stats.get(0);
		Assert.assertEquals(stat.getDate().toString(), result.getDate().toString());
		Assert.assertEquals(stat.getSev1(), result.getSev1());
		Assert.assertEquals(stat.getSev2(), result.getSev2());
		Assert.assertEquals(stat.getSev3(), result.getSev3());
		Assert.assertEquals(stat.getTotal(), result.getTotal());
		Assert.assertEquals(stat.getVerify(), result.getVerify());
		Assert.assertEquals(stat.getIncoming(), result.getIncoming());
		Assert.assertEquals(stat.getDeferred(), result.getDeferred());
		Assert.assertEquals(stat.getUntargeted(), result.getUntargeted());
	}

	@Test
	public void testParseAndSave() {
		String row = "12/9/2011,5,220,44,269,182,5,15,61";
		
		try {
			dao.parseAndSave(row);
			dao.parseAndSave(row);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<DailyStat> stats = dao.getAll();
		DailyStat result = stats.get(0);
		Assert.assertTrue(!stats.isEmpty());
	}
	
}
